//
//  main.cpp
//  threads_filosofo
//
//  Created by Cesar Adolfo Laura Mamani on 10/15/20.
//

#include <iostream>
#include <mutex>
#include <thread>
#include <utility>
using namespace std;

class Filosofos {
    int stomach;//
    int plato_fideo;
    Filosofos(){
  
    stomach =100;//
    plato_fideo=1000;//
  }
  
};

class Fork{//
  public:
    Fork(){;}
    mutex mu;
};

class timer{
  public:
    int time;
    auto endT(float timed){
      cout <<"\nLa cena duro "<< timed <<" S"<<endl;
    }
};
    
int main(int argc, const char * argv[]) {



    int number_of_philosophers;
      cout<<"Ingrese el numero de filosofos que estarán sentados en la mesa ► ";cin>>number_of_philosophers;cout<<endl;

      timer clock;
      clock.time=100000;
      int ms = clock.time;
      auto start = chrono::high_resolution_clock::now();

//------> Condicion 1 para lograr el deadlock, empezar con el estomago en 0 y plato en baja cantidad
      auto eat = [ms](Fork &left_Fork, Fork &right_Fork, int philosopher_number, int estomago=0, int plato_fideo=50){
       

        while(plato_fideo>0){

        if(estomago>100) {
          cout<<"► El filosofo "<<philosopher_number<<" ya no puede comer mas y pensará en momento\n"<<endl;
          estomago=estomago-100;
         
        }


        else if(estomago<=0){
          cout<<"+ El filosofo "<<philosopher_number<<" murio\n";
          break;
        } 
        else{

            //Una vez que comió ambos  el filosofo empieza a comer
            cout<<"→ El filosofo "<<philosopher_number<<" está comiendo.\n"<<endl;

            estomago=estomago+25;
            plato_fideo=plato_fideo-50;

          }
          //La variable ms dicta el tiempo que un filosofo termina de comer
          if(plato_fideo<=0){
            cout<<"✔ El filosofo " <<philosopher_number<<" termino de comer.\n"<<endl;
            break;
          }
          cout<<"○ El filosofo " <<philosopher_number<<" termino de comer una porcion.\n"<<endl;
        }

//------> Condicion 2 que no haya un control en los tiempos criticos

         //       unique_lock<mutex> llock(left_Fork.mu);
          // unique_lock<mutex> rlock(right_Fork.mu);  


            chrono::microseconds timeout(ms);
            this_thread::sleep_for(timeout);
      };
     
      Fork chp[number_of_philosophers];
  
      thread philosopher[number_of_philosophers];
      
      cout<<"... El filosofo 1 está pensando.\n"<<endl;
  



      philosopher[0] =thread(eat, ref(chp[0]),ref(chp[number_of_philosophers-1]), 1);
      
      for (int i = 1;i < number_of_philosophers;++i){
        cout<<"... El filosofo "<<(i+1)<<" está pensando.\n"<<endl;
        philosopher[i] = thread(eat,ref(chp[i]), ref(chp[i-1]),(i+1));
      }

      for (auto &ph: philosopher){
        ph.join();
      }
  
      auto end = chrono::high_resolution_clock::now();

      chrono::duration <float> duration = end - start;
      clock.endT(duration.count());
  
      return 0;   
}
   
