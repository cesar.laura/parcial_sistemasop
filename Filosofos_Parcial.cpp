//
//  main.cpp
//  threads_filosofo
//
//  Created by Cesar Adolfo Laura Mamani on 10/15/20.
//

#include <iostream>
#include <mutex>
#include <thread>
#include <utility>
using namespace std;

class Filosofos {
    int stomach;//
    int plato_fideo;
    Filosofos(){
  
    stomach =100;//
    plato_fideo=1000;//
  }
  
};

class Fork{//
  public:
    Fork(){;}
    mutex mu;
};

class timer{
  public:
    int time;
    auto endT(float timed){
      cout <<"\nLa cena duro "<< timed <<" S"<<endl;
    }
};
    
int main(int argc, const char * argv[]) {

 //****** B) Se pueden usar una cantidad cualquiera de filósofos definida por el usuario.

    int number_of_philosophers;
      cout<<"Ingrese el numero de filosofos que estarán sentados en la mesa ► ";cin>>number_of_philosophers;cout<<endl;

      timer clock;//Creamos una instancia en nuestro timer
      clock.time=1000;//Le damos este tiempo a nuestra instancia para encapsularla en la variable ms
      int ms = clock.time;
      auto start = chrono::high_resolution_clock::now();

//****** C) Todo filósofo debe tener un estomago   Y   H)   En la mesa debe haber varios platos de fideos.
//      Funcion auto eat, es una funcion labda que se encargara en la ejecusion de los mutex para la toma de tendores, donde 
//.     dependiendo de la situacion el thread de cada filosofo parada cuando este comiendo.      
      auto eat = [ms](Fork &left_Fork, Fork &right_Fork, int philosopher_number, int estomago=100, int plato_fideo=500, int contador = 0){
       

        while(plato_fideo>0){
//****** F) Cuando de un filósofo se llena su estomago, entonces debe dejar de comer. 
        if(estomago>100) {
          cout<<"► El filosofo "<<philosopher_number<<" ya no puede comer mas y pensará en momento\n"<<endl;
          estomago=estomago-25;
          contador ++;
          //cout<<"→ El filosofo "<<philosopher_number<<" está comiendo.\n";
        }

//****** G) Un filosofo se puede morir de hambre cuando su estomago esta cero(o menos de cero) por un determinado tiempo que puede esperar hambriento. 

        else if(estomago<=0){
          cout<<"+ El filosofo "<<philosopher_number<<" murio\n";
          break;
        } 
        else{

            //Una vez que comió ambos  el filosofo empieza a comer
            cout<<"→ El filosofo "<<philosopher_number<<" está comiendo.\n"<<endl;
//****** E) Se pueden usar una cantidad cualquiera de filósofos definida por el usuario.
            estomago=estomago+25;
         //   contador++;
            plato_fideo=plato_fideo-25;
            unique_lock<mutex> llock(left_Fork.mu);
            unique_lock<mutex> rlock(right_Fork.mu);  
            //Esperamos el tiempo encapsulado en la variable ms
//****** D) Cuando un fílosofo come se demora un tiempo aleatorio(Random), donde incrementa el valor de su estómago, basado en ms.   

            chrono::microseconds timeout(ms);
            this_thread::sleep_for(timeout);
          }
          //La variable ms dicta el tiempo que un filosofo termina de comer
          if(plato_fideo<=0){
            cout<<"✔ El filosofo " <<philosopher_number<<" termino de comer.\n"<<endl;
            cout<<"✔ El filosofo " <<philosopher_number<<" con su plato en: "<<plato_fideo<<" con su contador en: "<<contador<<endl;
            break;
          }
          cout<<"○ El filosofo " <<philosopher_number<<" termino de comer una porcion.\n"<<endl;
        }
      
      };
     
      Fork chp[number_of_philosophers];
  
      thread philosopher[number_of_philosophers];
      
      cout<<"... El filosofo 1 está pensando.\n"<<endl;
  


//****** A) Un filósofo es un Thread que pasa su vida pensando o comiendo, se le pasa los threads por cada
//          filosofo de manera que entra la funcion eat -> come y piensa , los treads de izquierda  y derecha representan los tenedores

      philosopher[0] =thread(eat, ref(chp[0]),ref(chp[number_of_philosophers-1]), 1);
      
      for (int i = 1;i < number_of_philosophers;++i){
        cout<<"... El filosofo "<<(i+1)<<" está pensando.\n"<<endl;
        philosopher[i] = thread(eat,ref(chp[i]), ref(chp[i-1]),(i+1));
      }

      for (auto &ph: philosopher){
        ph.join();
      }
  
      auto end = chrono::high_resolution_clock::now();

      chrono::duration <float> duration = end - start;
      clock.endT(duration.count());
  
      return 0;
    
    
}
   
